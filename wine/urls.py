from django.conf import settings
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import path, include

from geography.views import IndexView, SearchResultsView

urlpatterns = [
    path('rosetta/', include('rosetta.urls')),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('geo/', include('geography.urls', namespace='geo')),
    path('', include('accounts.urls')),
    path('', IndexView.as_view(template_name="index.html"), name='index'),
    path('', include('django.contrib.auth.urls')),  # необоходимо для сброса пароля и прочих auth-плюх
    path('search/', SearchResultsView.as_view(), name='search_results'),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

# Add Django site authentication urls (for login, logout, password management)
# urlpatterns += [
#     path('account/', include('django.contrib.auth.urls')),
# ]