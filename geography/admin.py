from django.contrib import admin

# Register your models here.
from accounts.models import Account
from core.models import Image
from geography.models import Country, Region, Sign, QualityMark, Area

admin.site.register(Country)
admin.site.register(Region)
admin.site.register(Area)
admin.site.register(QualityMark)
admin.site.register(Sign)


