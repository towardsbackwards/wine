from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.forms import ModelForm, widgets
from django.urls import reverse_lazy

from geography.models import Country, Region, Area, QualityMark, Sign


class MySelectWidget(widgets.Select):

    template_name = 'forms/widgets/my_signform_select.html'

    def get_context(self, name, value, attrs):
        """Чтобы каждую ссылку (add new) не прописывать в шаблон -
        создал генерацию ссылок в контекст при отрисовке виджета
        по имени (важно, чтобы имя виджета совпадало с именем ссылки в urlconf).
        Переменная генерируется в контекст вышеуказанного шаблона"""
        context = super().get_context(name, value, attrs)
        context['add_link'] = reverse_lazy(f'geo:create_{name}')
        return context


class SignCreateForm(ModelForm):
    name = 'Sign creation form'
    button_label = 'SAVE'

    class Meta:
        model = Sign
        fields = ('country', 'region', 'area', 'quality_mark', 'sign')
        widgets = {'country': MySelectWidget,
                   'region': MySelectWidget,
                   'area': MySelectWidget,
                   'quality_mark': MySelectWidget}
    method = 'POST'
    process_url = reverse_lazy('geo:create_quality_sign')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

#  print(SignCreateForm.__mro__)


class CountryCreateForm(ModelForm):
    form_label = 'Country creation form'
    button_label = 'Save'

    class Meta:
        model = Country
        fields = '__all__'

    method = 'POST'
    process_url = reverse_lazy('geo:create_country')

    def __init__(self, *args, **kwargs):
        super(CountryCreateForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class RegionCreateForm(ModelForm):
    form_label = 'Region creation form'
    button_label = 'Save'

    class Meta:
        model = Region
        fields = ('country', 'name')

    method = 'POST'
    process_url = reverse_lazy('geo:create_region')

    def __init__(self, *args, **kwargs):
        super(RegionCreateForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class AreaCreateForm(ModelForm):
    form_label = 'Area creation form'
    button_label = 'Save'

    class Meta:
        model = Area
        fields = ('name', 'region')

    method = 'POST'
    process_url = reverse_lazy('geo:create_area')

    def __init__(self, *args, **kwargs):
        super(AreaCreateForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class QualityMarkCreateForm(ModelForm):
    form_label = 'Quality mark creation form'
    button_label = 'Save'

    class Meta:
        model = QualityMark
        fields = '__all__'

    method = 'POST'
    process_url = reverse_lazy('geo:create_quality_mark')

    def __init__(self, *args, **kwargs):
        super(QualityMarkCreateForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


"""
class RegionForm(ModelForm):
    class Meta:
        model = Region
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(RegionForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
        self.fields['name'].queryset = Region.objects.filter(country=Country.name)
"""


class LoginForm(AuthenticationForm):
    form_label = 'Login form'

    class Meta:
        model = User
        fields = ('username', 'password')

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'login_field_form'  # это чтобы Bootstrap работал