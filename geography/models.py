from django.db import models
from django.urls import reverse
from static.py.countries_list import COUNTRIES_LIST
from static.py.regions_list import REGIONS_LIST
from django.utils.translation import gettext_lazy as _


class Country(models.Model):
    """Common Country model"""
    class Meta:
        verbose_name_plural = "Countries"

    """Список развернул чтобы корректно отображалось в шаблоне, и корректно работал 
    поиск по неразвёрнутому списку"""
    COUNTRIES = [(item[0], item[1]) for item in COUNTRIES_LIST]
    name = models.CharField(_('Country name'), unique=True, choices=COUNTRIES, max_length=128)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('geo:view', kwargs={'pk': self.pk})


class Region(models.Model):
    """Common region model (depends on Country)"""
    class Meta:
        verbose_name_plural = "Regions"

    REGIONS = REGIONS_LIST
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    name = models.CharField(_('Region name'), max_length=128)

    def __str__(self):
        return self.name


class Area(models.Model):
    """Common area model (depends on region)"""
    class Meta:
        verbose_name_plural = "Areas"

    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    name = models.CharField(_('Area name'), max_length=128)

    def __str__(self):
        return self.name


class QualityMark(models.Model):
    """Common quality mark model (depends on Country, region and area)"""
    class Meta:
        verbose_name_plural = "Quality marks"

    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    name = models.CharField(_('Quality mark'), max_length=64)

    def __str__(self):
        return self.name


class Sign(models.Model):
    """Common "wine sign" model (depends on Country, region. area and quality mark)"""
    class Meta:
        verbose_name_plural = "Signs"

    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    quality_mark = models.ForeignKey(QualityMark, on_delete=models.CASCADE)
    sign = models.CharField(_('Wine sign'), max_length=1)
    name = models.CharField(_('Sign'), max_length=64)

    def __str__(self):
        return f'Sign {self.id} ({self.country}, {self.region}, {self.sign})'
