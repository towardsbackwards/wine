from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, CreateView

from core.forms import SearchForm
from geography.forms import SignCreateForm, CountryCreateForm, RegionCreateForm, \
    AreaCreateForm, QualityMarkCreateForm
from geography.models import Sign, Country, Region, Area, QualityMark
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


class IndexView(TemplateView):
    """Generic class for index page rendering"""
    template_name = "index.html"


def load_regions(request):
    """Function for dynamic loading fields of sign creation form"""

    country_id = request.POST.get('countryId')
    region_id = request.POST.get('regionId')
    area_id = request.POST.get('areaId')

    regions = Region.objects.filter(country_id=country_id).order_by('name')
    areas = Area.objects.filter(region_id=region_id).order_by('name')
    quality_marks = QualityMark.objects.filter(area_id=area_id).order_by('name')
    context = {
        'country_id': country_id,
        'region_id': region_id,
        'regions': regions,
        'areas': areas,
        'quality_marks': quality_marks,
    }
    return render(request, 'inc/dropdown_list_country.html', context)


class SignCreateView(PermissionRequiredMixin, CreateView):
    """Generic class for sign creation form rendering"""
    template_name = "create_sign.html"
    model = Sign
    form_class = SignCreateForm
    success_url = reverse_lazy('geo:create_sign')
    permission_required = 'accounts.create_sign'  # во-вторых, у пользователя должно быть отдельное право на создание

    def get_context_data(self, **kwargs):  # добавление сортированного списка разрешений для текущего пользователя
        context = super().get_context_data(**kwargs)
        perms = list(self.request.user.get_user_permissions())
        # for item in set(self.request.user.get_user_permissions()):
        #     print(item)
        current_permissions = [w.replace('.', ' - ').replace('_', ' ') for w in sorted(perms)]
        context['current_permissions'] = current_permissions
        return context


class CountryCreateView(CreateView):
    """Generic class for country creation form rendering"""
    template_name = "create_country.html"
    model = Country
    form_class = CountryCreateForm
    success_url = reverse_lazy('geo:create_country')


class RegionCreateView(CreateView):
    """Generic class for region creation form rendering"""
    template_name = "create_region.html"
    model = Region
    form_class = RegionCreateForm
    success_url = reverse_lazy('geo:create_region')


class AreaCreateView(CreateView):
    """Generic class for area creation form rendering"""
    template_name = "create_area.html"
    model = Area
    form_class = AreaCreateForm
    success_url = reverse_lazy('geo:create_area')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['country'] = Country
        return context


class QualityMarkCreateView(CreateView):
    """Generic class for quality mark creation form rendering"""
    template_name = "create_quality_mark.html"
    model = QualityMark
    form_class = QualityMarkCreateForm
    success_url = reverse_lazy('geo:create_area')


class WinesView(ListView):
    """Generic class for wines list page rendering"""
    model = Sign
    fields = '__all__'
    template_name = 'view.html'
    context_object_name = 'articles'  # template variable for pagination

    def get_queryset(self):
        """Paginator test (lists by 1 item)"""
        if self.request.user.active:
            articles = Sign.objects.all()
            paginator = Paginator(articles, 1)
            page = self.request.GET.get('page')
            try:
                articles = paginator.page(page)
            except PageNotAnInteger:
                # Returns first page if page is not an integer.
                articles = paginator.page(1)
            except EmptyPage:
                # Returns last page of results if page is out of range (e.g. 9999)
                articles = paginator.page(paginator.num_pages)
            return articles

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.active:
            context['form'] = SearchForm
            return context
        else:
            context['error_msg'] = 'Для испольования базы Вам необходимо активировать свою учетную запись!'
            return context


class SearchResultsView(ListView):
    """Simple database search"""
    model = Sign
    template_name = 'search_results.html'

    def get_queryset(self):
        query = self.request.GET.get('search_field')
        print(Sign.objects.all())
        object_list = Sign.objects.filter(
            Q(name__icontains=query) |
            Q(quality_mark__country__name__icontains=query) |
            Q(quality_mark__area__name__icontains=query)
        )
        return object_list