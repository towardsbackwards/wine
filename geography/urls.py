from django.contrib.auth.decorators import login_required
from django.urls import path

from geography.views import SignCreateView, WinesView, \
    CountryCreateView, RegionCreateView, AreaCreateView, QualityMarkCreateView, load_regions, IndexView

app_name = 'geography'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('create/sign', login_required(SignCreateView.as_view()), name='create_sign'),
    path('view/', login_required(WinesView.as_view()), kwargs={'slug': 'another_url'}, name='view'),
    path('create/country', login_required(CountryCreateView.as_view()), name='create_country'),
    path('create/region', login_required(RegionCreateView.as_view()), name='create_region'),
    path('create/area', login_required(AreaCreateView.as_view()), name='create_area'),
    path('create/quality_mark', login_required(QualityMarkCreateView.as_view()), name='create_quality_mark'),

    path('ajax/load-regions', load_regions, name='ajax_load_regions'),

]
