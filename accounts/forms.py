from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, PasswordResetForm

from accounts.models import Account

User = get_user_model()


class UserCreateForm(UserCreationForm, PasswordResetForm):
    email = forms.EmailField(max_length=200, help_text='Required')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'login_field_form'
            field.widget.attrs['style'] = 'float: right'

    def save(self, commit=True, **kwargs):
        user = self.instance  # instance - то, что мы сохраняем
        if not user.id:  # если пользователь НОВЫЙ, т.е. ещё не зарегистрированный
            user.active = False
            user = super(UserCreateForm, self).save()
            PasswordResetForm.save(self, **kwargs)
        return user


class UserActivateForm(forms.Form):  # просто делает юзера активным, при срабатывании через урлы
    # (при переходе юзера по ссылке активации)
    class Meta:
        model = User

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        self.user.active = False
        if commit:
            self.user.save()
        return self.user
