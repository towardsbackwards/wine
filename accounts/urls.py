from django.contrib.auth.decorators import login_required
from django.urls import path

from accounts.views import Login, Logout, RegisterView, ConfirmView

app_name = 'accounts'


urlpatterns = [
    path('login/', Login.as_view(), name='login'),
    path('logout/', Logout.as_view(next_page='/'), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('confirm/<uidb64>/<token>/', ConfirmView.as_view(), name='confirm'),
    #path('password_reset/', ConfirmView.as_view(), name='password_reset_confirm')
]