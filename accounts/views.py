from django import forms
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetView, PasswordResetConfirmView
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView
from django.contrib.auth import get_user_model
from accounts.forms import UserCreateForm, UserActivateForm
from accounts.models import Account

User = get_user_model()


class UserNotAuthMixin(UserPassesTestMixin):
    """Mixin which restricts actions (login, Register) if user is authenticated already"""
    url_redirect = '/'
    warning_message = _('Эта опция доступна только для не авторизованных пользователей!')

    def test_func(self):
        """If authenticated -> returns False"""
        return not self.request.user.is_authenticated

    def handle_no_permission(self):
        messages.warning(self.request, self.warning_message)
        return HttpResponseRedirect(self.url_redirect)


class UserAuthMixin(UserNotAuthMixin):
    """Mixin which restricts actions (protected links) if user is not authenticated"""
    warning_message = _('Для перехода по данной ссылке пожалуйста, залогиньтесь!')

    def test_func(self):
        """If not authenticated -> returns False"""
        return self.request.user.is_authenticated


class Login(UserNotAuthMixin, SuccessMessageMixin, LoginView):
    success_message = _('Вход успешно осуществлен')


class Logout(UserAuthMixin, LogoutView, SuccessMessageMixin):
    success_message = _('Выход успешно осуществлен')
    url_redirect = '/'


class RegisterView(UserNotAuthMixin, SuccessMessageMixin, CreateView, PasswordResetView):
    template_name = "registration/register.html"
    model = User
    form_class = UserCreateForm
    success_url = reverse_lazy('index')
    url_redirect = reverse_lazy('index')
    success_message = 'Вы успешно зарегистрированы. ' \
                      'Письмо с инструкцией по активации "отправлено" (лежит в папке temp).' \
                      'ВАЖНО: Перед тем, как заходить на сайт, необходимо активировать учетную запись!'
    email_template_name = 'registration/signup_email.html'


class ConfirmView(PasswordResetConfirmView):
    """
    Activation registration
    """
    extra_context = {'page_title': _('Подтверждение регистрации'), 'header_class': 'hero', }

    success_url = reverse_lazy('accounts:login')
    template_name = 'registration/confirm_page.html'
    form_class = UserActivateForm
    post_reset_login = True
    post_reset_login_backend = 'django.contrib.auth.backends.ModelBackend'
    INTERNAL_RESET_URL_TOKEN = 'set-active'
