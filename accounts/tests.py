from timeit import timeit

from django.test import TestCase

# Create your tests here.
# import math
#
#
# def root(y):
#     x = math.sqrt(y*y + 124)
#     return x
#
#
# for i in range(10000000):
#     root(i)
#     if root(i).is_integer():
#         print(f'x = {root(i)}, y = {i}')


class Human:
    def __init__(self, name='Chris', age=20):
        self.name = name
        self.age = age
        print('Human init')


class Child1(Human):
    def __init__(self):
        super().__init__()  # = Human.__init__(self)
        print('Child1 init')


class Child2(Human):
    def __init__(self):
        super().__init__()  # = Human.__init__(self)
        print('Child2 init')


class UnderChild(Child1, Child2):
    def __init__(self):
        super().__init__()
        print('Underchild')


a = UnderChild()
