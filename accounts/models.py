from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from core.models import Kernel
from django.utils.translation import gettext_lazy as _


class AccountManager(UserManager):
    """
    Object Manager, get manager sets from DB
    """
    def get_managers(self):
        return self.filter(is_staff=True)


class Account(Kernel, AbstractUser):
    class Meta:
        verbose_name = _("Пользователь")
        verbose_name_plural = _("Пользователи")
    """User model (inherits fields from 2 models - AbstractUser and Kernel)"""
    first_name = models.CharField(_('Имя пользователя'), max_length=32, blank=True)
    objects = AccountManager()

    # last_name
    # email
    # is_staff
    # is_active  - будет использоваться is_active AbstractUser
    # date_joined
    # title
    # description
    # sort
    # password
    # last_login
    # pictures

    def __str__(self):
        return self.username