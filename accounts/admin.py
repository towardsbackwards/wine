from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import Permission

from accounts.models import Account

# admin.site.register(Account)
from core.models import Image


class PictureInline(admin.TabularInline):
    """My own administrator class which adds PICTURE field in Country edit admin form"""
    model = Image
    fk_name = 'related_obj'


class AccountAdmin(admin.ModelAdmin):
    """My own administrator class which adds PICTURE field in Country edit admin form"""
    inlines = (PictureInline, )

    pass


admin.site.register(Account, AccountAdmin)
admin.site.register(Permission)