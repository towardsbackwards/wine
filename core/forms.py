from django import forms
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _


class SearchForm(forms.Form):
    """Форма для поиска по базе данных
    Находится в папке 'core', так как не привязана к конкретному
    приложению и может быть использована везде"""
    form_label = 'Поиск винишка'
    button_label = 'Найти!'
    process_url = reverse_lazy('search_results')
    method = 'GET'
    search_field = forms.CharField(required=True, min_length=2,
                                   max_length=128,
                                   label=_('Поиск'),
                                   help_text='Эта форма позволит вам быстро найти необходимое вино',
                                   widget=forms.CharField.widget(attrs=
                                   {
                                       'onchange': 'this.form.submit()',
                                       'placeholder': _('Что ищем?')
                                   }
                                   ))

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['style'] = 'display: block'
