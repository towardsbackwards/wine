from django.contrib import admin
from core.models import Kernel


class KernelAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')


admin.site.register(Kernel, KernelAdmin)