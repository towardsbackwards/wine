from django.db import models
from django.utils.translation import gettext_lazy as _


class Kernel(models.Model):
    """The main model that the rest models will contain"""
    class Meta:
        verbose_name = _('Базовая модель')
        verbose_name_plural = _('Базовые модели')

    title = models.CharField(_('Заголовок'), max_length=128, blank=True, null=True)
    description = models.TextField(_('Описание'), max_length=256, blank=True, null=True)
    sort = models.IntegerField(_('Номер для сортировки'), default=0, blank=True, null=False)
    active = models.BooleanField(_('Активен ли объект'), default=True, db_index=True)

    # def __str__(self):
    #     return f'{self.title}' if self.title else f'{self.__class__.__name__, self.pk}'

    def remove(self):
        self.active = False
        self.save()


class Image(models.Model):
    """The main image model that the rest models will contain
    (if them will use images)"""
    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'

    image = models.ImageField(upload_to='images')
    related_obj = models.ForeignKey(Kernel, verbose_name='images',
                                    related_name='images', on_delete=models.CASCADE)



