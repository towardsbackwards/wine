from django import template

register = template.Library()


@register.filter
def yell(value):
    """Just testing how my own template tag will work
    this filter (yell) adds text message after variable where is was applied"""
    return f'{value} (and this message is from your own template tag!)'
