$("#id_country").change(function () {  // активация скрипта при изменении поля country
  var url = $("#signForm").attr("data-regions-url");  // получаем url {% url 'geo:ajax_load_regions' %}
  var countryId = $(this).val();  // получаем ID выбранной страны (при селекте на странице)
  $.ajax({                       // инициализация AJAX
    url: url,                    // установка адреса запроса
    data: {
        'country': countryId,   // добавление countryId в GET параметры (для передачи во view)
    },
    success: function (data) {   // "data" = данные load_regions во view
        $("#id_region").html(data);  // замена значений поля region данными, полученными из dropdown_list_country.html
    },
  });
});

$("#id_region").change(function () {
  var url = $("#signForm").attr("data-regions-url");
  var regionId = $(this).val();
  $.ajax({
    url: url,
    data: {
        'region': regionId,
    },
    success: function (data) {
        $("#id_area").html(data);
    },
  });
});

$("#id_area").change(function () {
  var url = $("#signForm").attr("data-regions-url");
  var areaId = $(this).val();
  $.ajax({
    url: url,
    data: {
        'area': areaId,
    },
    success: function (data) {
        $("#id_quality_mark").html(data);
    },
  });
});