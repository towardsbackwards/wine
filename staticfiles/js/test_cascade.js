document.getElementById('id_country').addEventListener("change", function() { // ожидаем изменений в id_country
    var region_choices = document.getElementById("id_region");
    var xhr = new XMLHttpRequest();
    var url = document.getElementById("signForm").getAttribute('data-regions-url');
    var csrf_token = document.getElementsByName("csrfmiddlewaretoken")[0].value;
    var countryId = document.getElementById('id_country').value; // взяли id выбранной страны
    xhr.open("POST", url, false); // GET or POST
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded") // necessary for POST requests.
    xhr.send(`countryId=${countryId}&csrfmiddlewaretoken=${csrf_token}`);
    region_choices.innerHTML = xhr.responseText;
    console.log(url);
    console.log(xhr.responseText); // что рендерится
    console.log(csrf_token); // показывает csrf_token
});
document.getElementById('id_region').addEventListener("change", function() { // ожидаем изменений в id_region
    var area_choices = document.getElementById("id_area");
    var xhr = new XMLHttpRequest();
    var url = document.getElementById("signForm").getAttribute('data-regions-url');
    var csrf_token = document.getElementsByName("csrfmiddlewaretoken")[0].value;
    var regionId = document.getElementById('id_region').value; // взяли id выбранного региона
    xhr.open("POST", url, false); // GET or POST
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded") // necessary for POST requests.
    xhr.send(`regionId=${regionId}&csrfmiddlewaretoken=${csrf_token}`);
    area_choices.innerHTML = xhr.responseText;
});
document.getElementById('id_area').addEventListener("change", function() { // ожидаем изменений в id_area
    var quality_mark_choices = document.getElementById("id_quality_mark");
    var xhr = new XMLHttpRequest();
    var url = document.getElementById("signForm").getAttribute('data-regions-url');
    var csrf_token = document.getElementsByName("csrfmiddlewaretoken")[0].value;
    var areaId = document.getElementById('id_area').value; // взяли id выбранного региона
    xhr.open("POST", url, false); // GET or POST
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded") // necessary for POST requests.
    xhr.send(`areaId=${areaId}&csrfmiddlewaretoken=${csrf_token}`);
    quality_mark_choices.innerHTML = xhr.responseText;
});